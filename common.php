<?php
include 'db.php';

function getUserID($userID) {
	global $c;
	$results = $c->query("SELECT * FROM users WHERE user_id='" . $userID . "'");
	if ($results && $results->num_rows > 0) {
		$row = $results->fetch_assoc();
		return $row["id"];
	} else {
		echo 0;
	}
}

function generateRandomCode($length) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $string = '';
    for ($i = 0; $i < $length; $i++) {
        $string .= $characters[mt_rand(0, strlen($characters) - 1)];
    }
    return $string;
}

