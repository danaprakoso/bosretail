<?php
include 'db.php';
include 'common.php';

$userID = getUserID($_POST["user_id"]);
$latitude = doubleval($_POST["latitude"]);
$longitude = doubleval($_POST["longitude"]);
$items = explode(";", $_POST["items"]); //Separated by ";"
$code = "";
while (true) {
	$code = generateRandomCode(6);
	$results = $c->query("SELECT * FROM orders WHERE order_code='" . $code . "'");
	if ($results && $results->num_rows > 0) {
		continue;
	}
	break;
}
$code = strtoupper($code);
$orderID = $c->query("SELECT * FROM order_items")->num_rows+1;
foreach ($items as $item) {
	$c->query("INSERT INTO order_items (order_id, grocery_id) VALUES (" . $orderID . ", " . $item . ")");
}
$c->query("INSERT INTO orders (user_id, order_id, sender_id, latitude, longitude, date, status, order_code) VALUES (" . $userID . ", " . $orderID . ", 1, " . $latitude . ", " . $longitude . ", " . round(microtime(true)*1000) . ", 0, '" . $code . "')");
$id = mysqli_insert_id($c);
echo $id;
