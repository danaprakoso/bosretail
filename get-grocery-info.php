<?php
include 'db.php';
$groceryID = intval($_POST["grocery_id"]);
$results = $c->query("SELECT * FROM groceries WHERE id=" . $groceryID);
if ($results && $results->num_rows > 0) {
	$row = $results->fetch_assoc();
	echo json_encode($row);
} else {
	echo "";
}
